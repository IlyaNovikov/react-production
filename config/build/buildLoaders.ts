import webpack from "webpack";

export function buildLoaders(): webpack.RuleSetRule[] {

    const typescriptLoaders = {
            test: /\.tsx?$/, // регулярка для поиска файлов с расширением, которые необходимо пропустить через loader
            use: 'ts-loader', // лоадер, который обрабатывает эти файлы
            exclude: /node_modules/, // исключения для файлов, где лоадер использовать не нужно
        };

        return [
            typescriptLoaders,
        ]
}