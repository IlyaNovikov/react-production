import {BuildOptions} from "./types/config";
import webpack from "webpack";
import {buildLoaders} from "./buildLoaders";
import {buildResolvers} from "./buildResolvers";
import {buildPlugins} from "./buildPlugins";

export function buildWebpackConfig(options: BuildOptions): webpack.Configuration {
    const {mode, paths} = options
    return {
        mode, // два типа production - минимизирует файлы, development - для разработки
        entry: paths.entry,
        module: {
            rules: buildLoaders()   // конфигурация лоудеров (те файлы, которые выходят за рамки js, например: png, svg, css и тд) и их обработка
        },
        resolve: buildResolvers(),// расширение тех файлов, для которых мы не будем указывать расширение при импорте
        output: {
            filename: '[name].[contenthash].js',
            path: paths.build,
            clean: true
        },
        plugins: buildPlugins(options),

    };
}